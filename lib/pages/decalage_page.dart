import 'package:chiffrements/util/decalage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DecalagePage extends StatefulWidget {
  const DecalagePage({super.key});

  @override
  State<DecalagePage> createState() => _DecalagePageState();
}

class _DecalagePageState extends State<DecalagePage> {
  String message = "";
  String cryptogramme = "";
  int decalage = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(50),
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "TEXTE EN CLAIR",
            ),
            onChanged: (value) {
              setState(() {
                message = value;
              });
            },
          ),
          Container(
            height: 30,
          ),
          TextField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "DECALAGE",
            ),
            onChanged: (value) {
              setState(() {
                decalage = int.parse(value);
              });
            },
          ),
          Container(height: 30),
          Row(
            children: [
              Expanded(
                child: MaterialButton(
                  onPressed: () {
                    setState(() {
                      cryptogramme = chiffrer(message, decalage);
                    });
                  },
                  child: Text(
                    "calculer",
                    style: GoogleFonts.lobster(
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                  color: Color.fromARGB(255, 249, 172, 185),
                  height: 50,
                ),
              ),
            ],
          ),
          Container(
            height: 30,
          ),
          Text(
            "Resultat: $cryptogramme",
            style: GoogleFonts.dancingScript(
              fontSize: 28,
              color: Color.fromARGB(255, 45, 41, 41),
            ),
          )
        ],
      ),
    );
  }
}
