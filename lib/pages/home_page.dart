import 'package:chiffrements/pages/decalage_page.dart';
import 'package:chiffrements/pages/rsa_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageController pageController = PageController();
  String titre = "calcul rsa";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          titre,
          style: GoogleFonts.lobster(fontSize: 24, color: Colors.white),
        ),
        backgroundColor: Color.fromARGB(255, 249, 172, 185),
      ),
      body: PageView(
        controller: pageController,
        children: [
          RsaPage(),
          DecalagePage(),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Center(
                child: Image.asset('images/lock.png'),
              ),
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 249, 172, 185),
              ),
            ),
            ListTile(
              title: Text(
                "calcul rsa",
                style: GoogleFonts.dancingScript(
                  fontSize: 28,
                  color: Color.fromARGB(255, 45, 41, 41),
                ),
              ),
              onTap: () {
                setState(() {
                  titre = "calcul rsa";
                  pageController.animateToPage(0,
                      duration: Duration(milliseconds: 100),
                      curve: Curves.easeInOut);
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: Text(
                "décalage",
                style: GoogleFonts.dancingScript(
                  fontSize: 28,
                  color: Color.fromARGB(255, 45, 41, 41),
                ),
              ),
              onTap: () {
                setState(() {
                  titre = "decalage";
                  pageController.animateToPage(1,
                      duration: Duration(milliseconds: 100),
                      curve: Curves.easeInOut);
                  Navigator.pop(context);
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
