import 'package:chiffrements/util/rsa.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RsaPage extends StatefulWidget {
  const RsaPage({super.key});

  @override
  State<RsaPage> createState() => _RsaPageState();
}

class _RsaPageState extends State<RsaPage> {
  int p = 0;
  int q = 0;
  String resultat = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      child: Column(children: [
        Row(
          children: [
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Valeur de p",
                ),
                onChanged: ((value) {
                  setState(() {
                    p = int.parse(value);
                  });
                }),
              ),
            ),
            Container(width: 10),
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Valeur de q",
                ),
                onChanged: (value) {
                  setState(() {
                    q = int.parse(value);
                  });
                },
              ),
            ),
          ],
        ),
        Container(
          height: 20,
        ),
        Row(
          children: [
            Expanded(
              child: MaterialButton(
                onPressed: () {
                  setState(() {
                    resultat = calculCles(p, q);
                  });
                },
                child: Text(
                  "calculer",
                  style: GoogleFonts.lobster(
                    fontSize: 24,
                    color: Colors.white,
                  ),
                ),
                color: Color.fromARGB(255, 249, 172, 185),
                height: 50,
              ),
            ),
          ],
        ),
        Container(
          height: 30,
        ),
        Text("RESULTAT $resultat"),
      ],),
    );
  }
}