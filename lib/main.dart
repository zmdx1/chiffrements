import 'package:chiffrements/pages/decalage_page.dart';
import 'package:chiffrements/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'pages/rsa_page.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'chiffrement',
      home: HomePage(),
    ),
  );
}
