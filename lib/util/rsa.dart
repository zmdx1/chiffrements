int pgcd(int a, int b) {
  while ((a * b) != 0) {
    if (a > b) {
      a = a - b;
    } else {
      b = b - a;
    }
  }

  if (a == 0) {
    return b;
  } else {
    return a;
  }
}

String calculCles(int p, int q) {
  var n = p * q;
  var phi = (p - 1) * (q - 1);
  var e = 2;
  for (e = 2; e < phi; e++) {
    if (pgcd(e, phi) == 1) break;
  }

  String clePublique = "($e,$n)";

  int d = 0;
  while (((d * e) % phi) != 1 || d == e) {
    d++;
  }
  String clePrivee = "($d,$n)";

  return "clé publique: $clePublique, clé privée: $clePrivee";
}