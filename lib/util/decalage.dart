String chiffrer(String message, int decalage) {
  const listIndex = {
    "A": 0,
    "B": 1,
    "C": 2,
    "D": 3,
    "E": 4,
    "F": 5,
    "G": 6,
    "H": 7,
    "I": 8,
  };

  const alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I"];

  String cryptogramme = "";
  for (var i = 0; i < message.length; i++) {
    int index = listIndex[message[i].toUpperCase()]!;
    cryptogramme += alphabet[(index + decalage) % alphabet.length];
  }
  return cryptogramme;
}
